#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<time.h>
#include<random>
#include<windows.h>
#include<cstdlib>
#include<sstream>

using namespace std;

__int64 start = 0;
double f = 0.0;

int randomrange(int mn, int mx)
{
   static bool f = true;
   if ( f )
   {
      srand(time(nullptr));
      f = false;
   }
   return mn + rand() % (mn - mx);
}

string randomword ()
{
    stringstream obj ;
    int SIZE = randomrange(0,10) ;
    vector <char> randword (SIZE) ;

    for (int i = 0 ; i<SIZE ; i++)
        randword[i] = randomrange(97,122) ;

    for (int i = 0 ; i<SIZE ; i++)
        obj<<randword[i] ;
    return obj.str() ;
}
template <class input>

class searcher
{

private:
    int left, right, mid ; vector <input> words ; int counter ;
public:


int binarySearch (const input element )
{
    counter = 0 ;
    left = 0 ;
    right = words.size() - 1;

    while( left <= right )
    {
        counter++ ;
        mid = ( left + right ) / 2;
        if( words[mid] < element )
            {
            left = mid + 1;
            counter++ ;
            }
        else if( words[ mid ] > element )
                {
                right = mid - 1;
                counter++ ;
                }
            else
                {
                return mid;
                }
    }

return -1;
}

void loadData (string filename)
{
ifstream i ; string buffer ;
i.open(filename, ios::in) ;

    while (!i.eof())
    {
    getline(i,buffer, '\n' ) ;
    words.push_back(buffer) ;
    }
}

int testPerformance ()
{
return counter ;
}

int get_size ()
{
    return words.size() - 1 ;
}

input get_word (int pos)
{
return words[pos] ;
}

};

void start_time()
{
    LARGE_INTEGER l;
    if(!QueryPerformanceFrequency(&l))
        cout << "failed!";

    f = double(l.QuadPart)/1000000.0; //micro
    QueryPerformanceCounter(&l);
    start = l.QuadPart;
}

double end_time()
{
    LARGE_INTEGER l;
    QueryPerformanceCounter(&l);
    double tmp = double(l.QuadPart-start)/f;
    return tmp ;
}


int main()
{
string w ;
cout<<endl<<"enter the word to search for: " ;
cin>>w ;

searcher<string> s ;

cout<<"\nChoose the file with the following number of words: \n1- 10000 \n2- 20000 \n3- 30000 \n4- 80000 \n5- 1000000\n\n" ;

int r ; cin>>r ;

switch (r)
{
case 1:  s.loadData("10000word.txt") ; break ;
case 2:  (s.loadData("20000word.txt")) ; break ;
case 3:  (s.loadData("30000word.txt")) ; break ;
case 4:   (s.loadData("80000word.txt")) ; break ;
case 5:   (s.loadData("100000word.txt")) ; break ;
default: break ;
}

double total_time = 0 ; int total_counter = 0 ; double Average_T, Average_C ;

start_time() ;                    //time calculation
int position = s.binarySearch(w) ;
double time = end_time() ;

if (position != -1)
{

cout<<"The word/ "<<s.get_word(position)<<" / Has been found at position / "<<position<<" / In time / "<<time<<" ms"<<endl ;


for (int i = 0 ; i<100 ; i++)
{
int rand = randomrange(0, s.get_size()) ;
w = s.get_word(rand) ;
start_time() ;
s.binarySearch(w) ;
time = end_time() ;
total_counter +=s.testPerformance() ;
total_time += time ;
cout<<"The word/ "<<w<<" / Has been found at position / "<<rand<<" / In time / "<<time<<" ms"<<endl ;
}

Average_T = total_time/100 ; Average_C = total_counter/100 ;

}

else
{
    cout<<"The non-existing word / " << w <<" / took time / " << time << endl ;
    int COUNTER = 0 ;
    while (COUNTER<100)
    {
    string randword = randomword() ;
    start_time() ;
    position = s.binarySearch(randword) ;
    time = end_time() ;
    if (position==-1)
    {
        COUNTER++ ;
        total_counter += s.testPerformance() ;
        total_time += time ;
        cout<<"The non-existing word / " << randword <<" / took time / " << time << endl ;
    }
    Average_C = total_counter/100 ; Average_T = total_time/100 ;
    }

}

cout<<endl<<"Average Number of comparisons is: " <<Average_C<<endl ;
cout << "Average Time: "<< Average_T  << " ms"<<endl ;

return 0;
}
